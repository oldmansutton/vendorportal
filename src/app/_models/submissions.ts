export class Submissions {
    VendorID: string;
    SubID: number;
    SubDescription: string;
    SubStartDate: Date;
    SubEndDate: Date;
    SubApprovedDate: Date;
    SubApproved: boolean;
    SubLastUpdate: Date;
    SubSubmittedUserID: number;
    SubApprovedUserID: number;
    SubTypeID: number;
}