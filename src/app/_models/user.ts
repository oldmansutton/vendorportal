export class User {
    UserID: number;
    FirstName: string;
    LastName: string;
    EmailAddress: string;
    Password: string;
    RoleID: string;
    VendorID: string;
    Approved: boolean;
    Token: string;
}