import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { MessageService } from 'primeng/components/common/api';

@Injectable({ providedIn: 'root' })
export class AlertService {
    constructor(private router: Router, private messageService: MessageService) { }

    success(message: string, isSticky = false) {
        this.messageService.add({severity: 'success', summary: 'Success', detail: message, sticky: isSticky})
    }

    info(message: string, isSticky = false) {
        this.messageService.add({severity: 'info', summary: 'Info', detail: message, sticky: isSticky})
    }

    warning(message: string, isSticky = false) {
        this.messageService.add({severity: 'warning', summary: 'Warning', detail: message, sticky: isSticky})
    }

    error(message: string, isSticky = false) {
        this.messageService.add({severity: 'error', summary: 'Error', detail: message, sticky: isSticky})
    }

}