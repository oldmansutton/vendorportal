import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { configVars } from '../configVars';

import { User } from '../_models/user';

@Injectable({ providedIn: 'root'})
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${configVars.apiUrl}/api/Users`);
    }

    getById(id: number) {
        return this.http.get(`${configVars.apiUrl}/api/Users/${id}`);
    }

    register(user: User) {
        return this.http.post(`${configVars.apiUrl}/api/Users/Register`, user);
    }

    update(user: User) {
        return this.http.put(`${configVars.apiUrl}/api/User/${user.UserID}`, user);
    }

    delete(id: number) {
        return this.http.delete(`${configVars.apiUrl}/Users/${id}`);
    }
}